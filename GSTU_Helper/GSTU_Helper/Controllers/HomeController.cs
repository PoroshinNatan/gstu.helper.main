﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GSTU_Helper.BusinessLayer.ChatBot.Services;
using GSTU_Helper.BusinessLayer.Models;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using GSTU_Helper.Extensions;
using GSTU_Helper.ViewModels.CommandManagement;
using GSTU_Helper.ViewModels.Global;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace GSTU_Helper.Controllers
{
    public class HomeController : Controller
    {
        private const int DefaultDisplayCount = 8;
        private const int DefaultPage = 1;

        private readonly IStringLocalizer _localizer;
        private readonly IBotService _bot;
        private readonly IRepository _repository;

        public HomeController(
            IStringLocalizer<HomeController> localizer,
            IBotService bot,
            IRepository repository
            )
        {
            _localizer = localizer;
            _bot = bot;
            _repository = repository;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        
    }
}