﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GSTU_Helper.BusinessLayer.ChatBot.Services;
using GSTU_Helper.BusinessLayer.Models;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using GSTU_Helper.Extensions;
using GSTU_Helper.ViewModels.CommandManagement;
using GSTU_Helper.ViewModels.Global;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace GSTU_Helper.Controllers
{
    public class CommandsController : Controller
    {
        private const int DefaultDisplayCount = 8;
        private const int DefaultPage = 1;

        private readonly IStringLocalizer _localizer;
        private readonly IBotService _bot;
        private readonly IRepository _repository;

        public CommandsController(
            IStringLocalizer<HomeController> localizer,
            IBotService bot,
            IRepository repository
            )
        {
            _localizer = localizer;
            _bot = bot;
            _repository = repository;
        }

        [Authorize]
        public IActionResult Index(PaginModel paginModel = null)
        {
            var model = new CommandsViewModel
            {
                Commands = _repository.CommandService.GetAll().ToList()
            };

            if (paginModel == null)
            {
                model.Pagin = model.Commands.ToPaginModel(DefaultDisplayCount, DefaultPage);
                model.Commands = model.Commands.GetItemsByPaginModel(model.Pagin).ToList();
            }
            else
            {
                model.Pagin = model.Commands.ToPaginModel(paginModel.DisplayCount, paginModel.CurrentPage);
                model.Commands = model.Commands.GetItemsByPaginModel(paginModel).ToList();
            }

            return PartialView("/Views/Commands/Commands.cshtml", model);
        }

        [Authorize]
        [HttpPost]
        public IActionResult AddCommand(Command command)
        {
            _repository.CommandService.Add(command);

            return Json(new {
                message = $"Команда {command.CommandName} успешно добавлена!",
                success = true
            });
        }

        [Authorize]
        [HttpPost]
        public IActionResult DeleteCommand(int? id)
        {
            if (!id.HasValue)
                return Json(new { message = "Неверный Id комманды!", success = false });

            var command = _repository.CommandService.Get(id.Value);

            try
            {
                _repository.CommandService.Delete(command);
            }
            catch (Exception ex)
            {
                return Json(new { message = "При удалении приозошла ошибка!", success = false });
            }

            return Json(new { message = $"Комманда {command.CommandName} была успешно удалена!", success = true });
        }

        [Authorize]
        [HttpPost]
        public IActionResult Activate(int? id)
        {
            if (!id.HasValue)
                return Json(new { message = "Неверный Id комманды!", success = false });

            var command = _repository.CommandService.Get(id.Value);

            try
            {
                _repository.CommandService.ActivateCommand(id.Value);
            }
            catch(Exception ex)
            {
                return Json(new { message = "При активации приозошла ошибка!", success = false });
            }

            return Json(new { message = $"Комманда {command.CommandName} была успешно активирована!", success = true });
        }

        [Authorize]
        [HttpPost]
        public IActionResult Disable(int? id)
        {
            if (!id.HasValue)
                return Json(new { message = "Неверный Id комманды!", success = false });

            var command = _repository.CommandService.Get(id.Value);

            try
            {
                _repository.CommandService.DeactivateCommand(id.Value);
            }
            catch (Exception ex)
            {
                return Json(new { message = "При деактивации приозошла ошибка!", success = false });
            }

            return Json(new { message = $"Комманда {command.CommandName} была успешно деактивирована!", success = true });
        }
    }
}