﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GSTU_Helper.Migrations
{
    public partial class Version3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Cathedras_LocationDetailsId",
                table: "Cathedras",
                column: "LocationDetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cathedras_LocationDetails_LocationDetailsId",
                table: "Cathedras",
                column: "LocationDetailsId",
                principalTable: "LocationDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cathedras_LocationDetails_LocationDetailsId",
                table: "Cathedras");

            migrationBuilder.DropIndex(
                name: "IX_Cathedras_LocationDetailsId",
                table: "Cathedras");
        }
    }
}
