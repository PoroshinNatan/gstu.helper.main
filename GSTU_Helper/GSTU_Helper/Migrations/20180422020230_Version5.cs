﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GSTU_Helper.Migrations
{
    public partial class Version5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Argument",
                table: "CommandArguments");

            migrationBuilder.AddColumn<int>(
                name: "ArgumentType",
                table: "CommandArguments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArgumentType",
                table: "CommandArguments");

            migrationBuilder.AddColumn<string>(
                name: "Argument",
                table: "CommandArguments",
                nullable: true);
        }
    }
}
