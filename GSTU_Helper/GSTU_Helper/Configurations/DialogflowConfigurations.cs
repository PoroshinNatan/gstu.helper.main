﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.Configurations
{
    public class DialogflowConfigurations
    {
        public string ClientApiToken { get; set; }

        public string DevelopApiToken { get; set; }
    }
}
