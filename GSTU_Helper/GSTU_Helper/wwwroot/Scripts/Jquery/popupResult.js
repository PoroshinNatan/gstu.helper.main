﻿
var Popup = {
    ShowMessage: function (message, success) {
        var text = $('<span>')
            .css('font', ' normal 24px/1.2 Verdana')
            .css('color', 'white')
            .css('text-shadow', '0px 0px 5px #000000');

        if (message.length > 150)
            text.empty()
                .append("Возникла ошибка");
        else
            text.append(message);

        var messageBox = $('<div>')
            .css('padding', '20px 60px 10px 60px')
            .css('position', 'fixed')
            .css('width', '600px')
            .css('min-height', '100px')
            .css('left', '35%')
            .css('top', '-800px')
            .css('z-index', '1000')
            .css('border', '1px solid black')
            .css('border-radius', '3px')
            .css('box-shadow', '0px 0px 27px -6px rgba(0,0,0,1)')
            .css('text-align','center')
            .append(text);


        if (success) {
            messageBox
                .css('background', 'rgb(72, 255, 57)');
        }
        else {
            messageBox
                .css('background', 'rgb(255, 0, 0)');
        }

        $('body').append(messageBox);

        messageBox.animate({
            top:'0'
        }, 1000, function () {
            var timerId = setTimeout(function () {
                messageBox.animate({
                    opacity: 0
                }, 1500, function () {
                    messageBox.remove();
                })
            }, 4000);
        });
    }
}