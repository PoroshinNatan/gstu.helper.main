﻿$(function () {

    var right = true;

    $('.js-open-menu').click(function (e) {
        if (right) {
            $(this).find($('svg'))
                .removeClass('fa-angle-double-right')
                .addClass('fa-angle-double-left');

            $('.menu').animate({
                left: '0px'
            }, 500);
        }
        else {
            $(this).find($('svg'))
                .removeClass('fa-angle-double-left')
                .addClass('fa-angle-double-right');

            $('.menu').animate({
                left: '-350px'
            }, 500);
        }
        right = !right;
    });

    $('.js-commands-button').on('click', function (e) {
        Commands.GetCommandsPage();
    });

    Commands.GetCommandsPage();
})