﻿
    //страница для списка и управления сообщениями
    var Messanging = {
        GetAllMessages: function () {

        }
    };

    //страница для списка и управления коммандами
    var Commands =  {
        CommandParams:[],

        GetCommandsPage: function () {
            var that = this;
            $('.ui-dialog').remove();

            this.GetCommandsOnPage(8, 1);
        },

        GetCommandsOnPage: function (count, page) {
            var insertData = $('.main-content-data');
            insertData.empty();

            $('.data-loading').css('display', 'block');
            var that = this;
            

            $.get({
                url: "/Commands/Index",
                data: {
                    displayCount: count,
                    currentPage: page  
                },
                success: function (data) {
                    insertData.append($(data));
                    that.InitCommandsPagin();
                    that.CreateCommandDialog();

                    $('.command-line').on('click', function (e) {

                        var dialog = $('.js-delete-command-dialog').dialog({
                            title: "Действие с коммандой",
                            width: 400,
                            buttons: [
                                {
                                    text: "Принять",
                                    click: function () {
                                        var id = $(e.target).closest('tr').data('id');
                                        var action = $("input[type='radio'][name='commandAction']:checked").val();

                                        if (action == 0)
                                            that.DeleteCommand(id);
                                        else if (action == 1)
                                            that.DisableCommand(id);
                                        else
                                            that.ActivateCommand(id);

                                        $(dialog).dialog('close');
                                    }
                                },
                                {
                                    text: "Отмена",
                                    click: function () {
                                        $(dialog).dialog('close');
                                    }
                                }
                            ],
                            modal: true,
                            draggable: true
                        });
                    })
                }
            }).fail(function (xhr, status, error) {
                $('.data-loading').css('display', 'none');
                Popup.ShowMessage(xhr.responseText, false);
            }).then(function () {
                $('.data-loading').css('display', 'none');
            });
        },

        InitCommandsPagin: function () {
            var pages = $('.page');
            var displayCount = $('.displaying-item');

            var currDisplayCount = $('.display-item.active').data('displaycount');
            var currPage = $('.page.active').data('page');
            var that = this;

            pages.on('click', function (e) {
                that.GetCommandsOnPage(currDisplayCount, $(this).data('page'));
            });

            displayCount.on('click', function (e) {
                that.GetCommandsOnPage($(this).data('displaycount'), currPage);
            })
        },

        CreateCommandDialog: function () {
            this.CommandParams = [];
            var button = $('.js-create-command-dialog-button');
            var that = this;

            $('.js-add-parameter').click(function () {
                var commandParameter = {
                    ParameterName: $("input[name='commandParameter-name']").val(),
                    Description: $("textarea[name='commandParameter-description']").val(),
                    ArgumentType: $("input[type='radio'][name='command-parameter-type']:checked").val()
                };

                that.CommandParams.push(commandParameter);

                var commandParameterLine = $('<div>')
                    .addClass('command-parameter-line')
                    .text(commandParameter.Description);

                $('.commandParameter-block').append(commandParameterLine);

            });

            var dialog = $('.js-create-command-dialog')
                .dialog({
                    modal: true,
                    width: 800,
                    autoOpen: false,
                    title: "Создание комманды",
                    draggable: false,
                    buttons: [
                        {
                            text: "Создать команду",
                            click: function () {
                                var command = {
                                    CommandName: $("input[type='text'][name='commandName']").val(),
                                    Description: $("textarea[name='commandDescription']").val(),
                                    CommandPattern: $("input[type='text'][name='commandPattern']").val(),
                                    CommandParameters: that.CommandParams
                                };

                                $.post({
                                    data: command,
                                    url: '/Commands/AddCommand',
                                    success: function (response) {
                                        Popup.ShowMessage(response.message, response.success);
                                        $(dialog).dialog("close");

                                        that.GetCommandsPage();
                                    }
                                });
                            }
                        },
                        {
                            text: "Закрыть",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ]
            });

            button.on('click', function (e) {
                dialog.dialog("open");
            })
        },

        DeleteCommand: function (commandId) {
            var that = this;

            $.post({
                url: "/Commands/DeleteCommand",
                data: { id: commandId },
                success: function (response) {
                    Popup.ShowMessage(response.message, response.success);
                    that.GetCommandsPage();
                }
            })
        },

        ActivateCommand: function (commandId) {
            var that = this;

            $.post({
                url: "/Commands/Activate",
                data: { id: commandId },
                success: function (response) {
                    Popup.ShowMessage(response.message, response.success);
                    that.GetCommandsPage();
                }
            });
        },

        DisableCommand: function (commandId) {
            var that = this;

            $.post({
                url: "/Commands/Disable",
                data: { id: commandId },
                success: function (response) {
                    Popup.ShowMessage(response.message, response.success);
                    that.GetCommandsPage();
                }
            });
        }
    };
