﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.ChatBot
{
    public class BaseChatBotException : Exception
    {
        public BaseChatBotException(string message)
            :base(message)
        {

        }
    }
}
