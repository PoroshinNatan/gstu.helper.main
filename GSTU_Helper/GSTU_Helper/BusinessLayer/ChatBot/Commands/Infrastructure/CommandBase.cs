﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot;
using GSTU_Helper.BusinessLayer.Models;
using Telegram.Bot.Args;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using GSTU_Helper.BusinessLayer.ChatBot.Commands.Infrastructure.Exceptions;

namespace GSTU_Helper.BusinessLayer.ChatBot.Commands.Infrastructure
{
    public abstract class CommandBase
    {
        public CommandBase(Command command, IRepository repository)
        {
            Command = command;
            Repository = repository;
        }

        public virtual Command Command { get; set; }

        protected IRepository Repository { get; set; }

        public abstract void Execute(IDictionary<string,string> args, TelegramBotClient client, Message message);

        public virtual void ValidateArgs(IDictionary<string,string> args)
        {
            if(args.Count != 0 && Command.CommandParameters == null)
            {
                throw new InvalidCommandArgumetsException("Для этой команды параметры не нужны.");
            }

            if(args.Count > Command.CommandParameters.Count)
            {
                throw new InvalidCommandArgumetsException("Указаны неверные параметры команды.\n Пожалуйста, посмотрите описание введенной вами команды:\n" + Command.GetCommandDescription());
            }

        }
    }
}
