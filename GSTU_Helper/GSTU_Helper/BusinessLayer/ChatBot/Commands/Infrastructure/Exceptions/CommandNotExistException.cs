﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.ChatBot.Commands.Infrastructure.Exceptions
{
    public class CommandNotExistException : BaseChatBotException
    {
        public CommandNotExistException(string message)
            : base(message)
        {

        }
    }
}
