﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.ChatBot.Commands.Infrastructure.Exceptions
{
    public class InvalidCommandArgumetsException : BaseChatBotException
    {
        public InvalidCommandArgumetsException(string message)
            :base(message)
        {
            
        }
    }
}
