﻿using GSTU_Helper.BusinessLayer.ChatBot.Commands.Infrastructure;
using GSTU_Helper.BusinessLayer.ChatBot.Commands.Infrastructure.Exceptions;
using GSTU_Helper.BusinessLayer.Models;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;

namespace GSTU_Helper.BusinessLayer.ChatBot.Commands
{
    public class HelpCommand : CommandBase
    {
        public HelpCommand(Command command, IRepository repository)
            : base(command, repository)
        {

        }
        public override async void Execute(IDictionary<string, string> args, TelegramBotClient client, Message message)
        {
            ValidateArgs(args);

            if(args.Count == 0)
            {
                await client.SendTextMessageAsync(message.Chat.Id, Command.GetCommandDescription());
            }

            
            //-a
            if(args.ContainsKey(Command.CommandParameters[0].ParameterName))
            {
                await client.SendTextMessageAsync(message.Chat.Id, "Инструкция еще в разработке.");
            }
        }
    }
}
