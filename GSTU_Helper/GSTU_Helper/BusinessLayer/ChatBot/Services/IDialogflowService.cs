﻿using ApiAiSDK.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.ChatBot.Services
{
    public interface IDialogflowService
    {
        AIResponse SendTextRequest(string text);

        AIResponse SendTextRequest(AIRequest request);

        AIResponse SendVoiceRequest(Stream voiceRequest);
    }
}
