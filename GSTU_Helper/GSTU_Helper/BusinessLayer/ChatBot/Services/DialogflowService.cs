﻿using ApiAiSDK;
using ApiAiSDK.Util;
using ApiAiSDK.Model;
using GSTU_Helper.Configurations;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.ChatBot.Services
{
    public class DialogflowService : IDialogflowService
    {
        private const string UnreadableMessage = "ahsdjhkj1j3h8asfyasdsd8asflsb2313l12b3v1k4c13123189sad9a";

        private readonly AIConfiguration _aiConfiguration;
        private readonly ApiAi _apiAi;
        private readonly IOptions<DialogflowConfigurations> _configs;

        public DialogflowService(IOptions<DialogflowConfigurations> configs)
        {
            _configs = configs;
            _aiConfiguration = new AIConfiguration(_configs.Value.ClientApiToken, SupportedLanguage.Russian);
            _apiAi = new ApiAi(_aiConfiguration);
        }

        public AIResponse SendTextRequest(AIRequest request)
        {
            return _apiAi.TextRequest(request);
        }

        public AIResponse SendTextRequest(string text)
        {
            if (string.IsNullOrEmpty(text))
                return _apiAi.TextRequest(UnreadableMessage);

            return _apiAi.TextRequest(text);
        }

        public AIResponse SendVoiceRequest(Stream voiceRequest)
        {
            return _apiAi.VoiceRequest(voiceRequest);
        }
    }
}
