﻿using GSTU_Helper.BusinessLayer.ChatBot.Commands.Infrastructure;
using GSTU_Helper.BusinessLayer.ChatBot.Commands.Infrastructure.Exceptions;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace GSTU_Helper.BusinessLayer.ChatBot.Services
{
    public class CommandService : ICommandService
    {
        private const string CommandPostfix = @"Command";
        private const string CommandFirstPart = @"\/\w";
        private const string IsCommand = @"^(\/\w{1,})";
        private const string IsContainDigit = @"\d{1,}";
        private const string SpaceSeparator = @"\s{1,}";
        private const string IsArg = @"^(-\w{1,})";
        private const string IsArgParamener = @"^(\w{1,})";
        private const string CommandsNamespace = @"GSTU_Helper.BusinessLayer.ChatBot.Commands";

        private IReadOnlyCollection<CommandBase> _commands;
        private IRepository _repository;

        public CommandService(IRepository repository)
        {
            _repository = repository;

            var comList = _repository
                .CommandService
                .GetAll()
                .Select(x => {
                    var commands = Assembly.GetExecutingAssembly()
                    .GetTypes();

                    var commandType = commands
                    .FirstOrDefault(type => type.IsClass
                    && type.Namespace == CommandsNamespace
                    && ConvertCommandNameToCommandClass(x.CommandName) == type.Name);

                    if (commandType != null)
                    {
                        return (CommandBase)commandType
                        .GetConstructors()
                        .First(y => y.GetParameters().Count() > 0)
                        .Invoke(new object[] { x, repository });
                    }
                    else
                        return null;
                })
                .ToList();

            comList.RemoveAll(item => item == null);
            _commands = comList;
        }

        public bool ExecuteIfExist(Message message, TelegramBotClient botClient)
        {
            if (!string.IsNullOrEmpty(message.Text))
            {
                var commandName = GetCommand(message.Text);
                var args = GetArgs(message.Text);

                if (!string.IsNullOrEmpty(commandName) && _commands != null)
                {
                    foreach (var command in _commands)
                    {
                        if (command.Command.CommandName == commandName && command.Command.IsActive.Value)
                        {
                            command.Execute(args, botClient, message);
                            return true;
                        }

                        if (command.Command.CommandName == commandName && !command.Command.IsActive.Value)
                        {
                            throw new CommandDisabledException("Данная команда существует, но была отключена администраторами.");
                        }
                    }
                    throw new CommandNotExistException("Команды не существует");
                }
            }

            return false;
        }

        private string GetCommand(string textMessage)
        {
            var reg = new Regex(IsCommand);

            return reg.Match(textMessage).Value;
        }

        private IDictionary<string, string> GetArgs(string textMessage)
        {
            var reg = new Regex(SpaceSeparator);

            var argsValues = reg
                .Split(textMessage)
                .Skip(1)
                .ToList();

            var dictionary = new Dictionary<string, string>();

            reg = new Regex(IsArg);

            //Добавление ключ-пар аргументов и их параметров
            for(int i = 0; i < argsValues.Count; i++)
            {
                if (reg.IsMatch(argsValues[i]))
                {
                    if (i != argsValues.Count - 1)
                    {
                        if (!reg.IsMatch(argsValues[i + 1]))
                        {
                            dictionary.Add(argsValues[i],argsValues[i+1]);
                            i++;
                        }
                        else
                        {
                            dictionary.Add(argsValues[i], null);
                        }
                    }
                    else
                    {
                        dictionary.Add(argsValues[i], null);
                    }
                }
            }

            return dictionary;
        }

        private string ConvertCommandNameToCommandClass(string commandName)
        {
            var reg = new Regex(CommandFirstPart);

            var finalValue = reg.Replace(commandName, x => {
                return Char.ToUpper(x.Value[1]).ToString();
            });
            return finalValue + CommandPostfix;
        }
    }
}
