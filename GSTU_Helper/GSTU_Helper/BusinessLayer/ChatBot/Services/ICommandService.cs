﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace GSTU_Helper.BusinessLayer.ChatBot.Services
{
    public interface ICommandService
    {
        bool ExecuteIfExist(Message message, TelegramBotClient botClient);
    }
}
