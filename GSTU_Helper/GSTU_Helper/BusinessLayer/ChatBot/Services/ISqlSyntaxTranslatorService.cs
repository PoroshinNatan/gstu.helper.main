﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.ChatBot.Services
{
    public interface ISqlSyntaxTranslatorService
    {
        bool TryTranslate(string message);

        string GetResponse(string message);
    }
}
