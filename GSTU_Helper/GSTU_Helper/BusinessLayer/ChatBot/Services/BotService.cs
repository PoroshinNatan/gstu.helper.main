﻿using GSTU_Helper.Configurations;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;

namespace GSTU_Helper.BusinessLayer.ChatBot.Services
{
    public class BotService : IBotService
    {
        private readonly TelegramBotClient _client;
        private readonly IOptions<BotConfigurations> _configs;
        private readonly ICommandService _commandService;
        private readonly IDialogflowService _dialogflowService;
        private readonly ISqlSyntaxTranslatorService _sqlSyntaxTranslatorService;

        public BotService(
            IOptions<BotConfigurations> configs,
            ICommandService commandService,
            IDialogflowService dialogflowService
            )
        {
            _configs = configs;
            _client = new TelegramBotClient(_configs.Value.ApiToken);
            _commandService = commandService;
            _dialogflowService = dialogflowService;

            _client.OnMessage += OnMessageReceived;
            _client.StartReceiving();
        }

        private async void OnMessageReceived(object sender, MessageEventArgs eventArgs)
        {
            var message = eventArgs.Message;

            try
            {
                if(!_commandService.ExecuteIfExist(message, _client))
                {
                    ApiAiSDK.Model.AIResponse feedback;

                    if (message.Type == Telegram.Bot.Types.Enums.MessageType.VoiceMessage)
                    {
                        var file = await GetFileStreamByFileId(message);
                        //Пока что в .net core не доступно преобразование голоса в строку
                        feedback = _dialogflowService.SendTextRequest(eventArgs.Message.Text);
                    }
                    else
                    {
                        feedback = _dialogflowService.SendTextRequest(eventArgs.Message.Text);
                    }

                    await _client.SendTextMessageAsync(message.Chat.Id, feedback.Result.Fulfillment.Speech);
                }
            }
            catch(BaseChatBotException ex)
            {
                await _client.SendTextMessageAsync(message.Chat.Id, ex.Message);
            }
        }

        private async Task<Telegram.Bot.Types.File> GetFileStreamByFileId(Message message)
        {
            return await _client.GetFileAsync(message.Voice.FileId);
        }
    }
}
