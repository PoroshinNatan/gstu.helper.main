﻿using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.ChatBot.Services
{
    public class SqlSyntaxTranslatorService : ISqlSyntaxTranslatorService
    {
        private GstuHelperDbContext _context;


        public SqlSyntaxTranslatorService(GstuHelperDbContext context)
        {
            _context = context;
        }

        public string GetResponse(string message)
        {
            var response = string.Empty;

            try
            {

            }
            catch(Exception ex)
            {
                response = ex.Message;
            }

            return response;
        }

        public bool TryTranslate(string message)
        {
            bool canTranslated = false;

            try
            {

            }
            catch (Exception ex)
            {
                canTranslated = false;
            }

            return canTranslated;
        }
    }
}
