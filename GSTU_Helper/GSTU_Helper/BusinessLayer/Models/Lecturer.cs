﻿using GSTU_Helper.BusinessLayer.Models.Enums;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class Lecturer : BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public ScientistGrade ScientistGrade { get; set; }
    }
}
