﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Enums
{
    public enum ScientistGrade
    {
        [Display(Description = "Неизвестно")]
        Unknown,

        [Display(Description = "Отсутствует")]
        None,

        [Description("Глава кафедры")]
        [Display(Description = "Глава кафедры")]
        ChathedraHead,

        [Description("Аспирант")]
        [Display(Description = "Аспирант")]
        GraduateStudent,

        [Description("Доцент")]
        [Display(Description = "Доцент")]
        Docent,

        [Description("Профессор")]
        [Display(Description = "Профессор")]
        Professor,

        [Description("Ст. Преподаватель")]
        [Display(Description = "Старший преподаватель")]
        SeniorLecturer,

        [Description("Ассистент")]
        [Display(Description = "Ассистент")]
        Assistance
    }
}
