﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Enums
{
    public enum ArgumentType
    {
        None = 0,
        Int = 1,
        String = 2
    }
}
