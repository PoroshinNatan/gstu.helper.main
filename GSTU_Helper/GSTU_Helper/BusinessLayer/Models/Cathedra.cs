﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class Cathedra : BaseModel
    {
        public string Name { get; set; }
        public Lecturer CathedraHead { get; set; }
        public CommunicationInfo CommunicationInfo { get; set; }
        public Faculty Faculty { get; set; }
        public LocationInfo LocationDetails { get; set; }
        public IList<Lecturer> CathedraStaff { get; set; }
        public IList<Speciality> Specialities { get; set; }
    }
}
