﻿using GSTU_Helper.BusinessLayer.Models.Enums;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class CommandParameter : BaseModel
    {
        public string ParameterName { get; set; }

        public string Description { get; set; }
        public ArgumentType ArgumentType { get; set; }
    }
}
