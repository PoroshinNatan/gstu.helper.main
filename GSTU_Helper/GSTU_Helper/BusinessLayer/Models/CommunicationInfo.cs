﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class CommunicationInfo : BaseModel
    {
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
