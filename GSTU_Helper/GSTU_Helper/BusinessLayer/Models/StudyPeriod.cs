﻿using GSTU_Helper.BusinessLayer.Models.Enums;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class StudyPeriod : BaseModel
    {
        public StudyFormType FormType { get; set; }
        public int? Period { get; set; }
    }
}
