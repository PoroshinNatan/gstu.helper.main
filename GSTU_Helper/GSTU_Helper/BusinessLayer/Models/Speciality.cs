﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class Speciality : BaseModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Qualification { get; set; }
        public string Description { get; set; }
        public IList<string> ActivityFields { get; set; }
        public IList<Discipline> Disciplines { get; set; }
        public IList<StudyPeriod> StudyPeriods { get; set; }
        public IList<Specialization> Specializations { get; set; }
    }
}
