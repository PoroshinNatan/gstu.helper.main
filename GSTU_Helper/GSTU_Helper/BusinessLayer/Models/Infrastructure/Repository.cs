﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces;
using GSTU_Helper.DataLayer.DataModels;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure
{
    public class Repository : IRepository
    {
        public Repository(
            ICommandService commandService,
            ICathedraService cathedraService
            )
        { 
            CommandService = commandService;
            CathedraService = cathedraService;
        }

        public ICommandService CommandService { get; set; }

        public ICathedraService CathedraService { get; set; }
    }
}
