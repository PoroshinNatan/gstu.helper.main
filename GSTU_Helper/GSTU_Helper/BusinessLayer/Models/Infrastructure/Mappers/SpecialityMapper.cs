﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class SpecialityMapper : ISpecialityMapper
    {
        private readonly IDisciplineMapper _disciplineMapper;
        private readonly IStudyPeriodMapper _studyPeriodMapper;
        private readonly ISpecializationMapper _specializationMapper;

        public SpecialityMapper(
            IDisciplineMapper disciplineMapper,
            IStudyPeriodMapper studyPeriodMapper,
            ISpecializationMapper specializationMapper
            )
        {
            _disciplineMapper = disciplineMapper;
            _studyPeriodMapper = studyPeriodMapper;
            _specializationMapper = specializationMapper;
        }

        public Speciality Map(Specialities model)
        {
            var speciality = new Speciality
            {
                Id = model.Id,
                ActivityFields = model.SpecialityActivityFields.Select(x => x.ActivityField.Name).ToList(),
                Code = model.Code,
                Name = model.Name,
                Description = model.Description,
                Disciplines = model.SpecialityStudyDisciplines
                    .Select(x => _disciplineMapper.Map(x.StudyDiscipline))
                    .ToList(),
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                Qualification = model.Qualification,
                Specializations = model.Specializations
                    .Select(x => _specializationMapper.Map(x))
                    .ToList(),
                StudyPeriods = model.SpecialityStudyPeriods
                    .Select(x => _studyPeriodMapper.Map(x.StudyPeriod))
                    .ToList()
            };

            return speciality;
        }

        public Specialities Map(Speciality model)
        {
            throw new NotImplementedException();
        }
    }
}
