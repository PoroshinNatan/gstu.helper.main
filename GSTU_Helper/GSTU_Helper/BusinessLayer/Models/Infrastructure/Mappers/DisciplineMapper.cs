﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class DisciplineMapper : IDisciplineMapper
    {
        public Discipline Map(StudyDisciplines model)
        {
            var discipline = new Discipline
            {
                Id = model.Id,
                CreatedAt = model.CreatedAt,
                Description = model.Description,
                Difficulty = model.Difficulty,
                ModifiedAt = model.ModifiedAt
            };

            return discipline;
        }

        public StudyDisciplines Map(Discipline model)
        {
            var disciplineDbModel = new StudyDisciplines
            {
                Id = model.Id,
                Description = model.Description,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                Difficulty = model.Difficulty
            };

            return disciplineDbModel;
        }
    }
}
