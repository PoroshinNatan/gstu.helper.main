﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class CathedraMapper : ICathedraMapper
    {
        private readonly ILecturerMapper _lecturerMapper;
        private readonly ICommunicationInfoMapper _communicationInfoMapper;
        private readonly ILocationInfoMapper _locationInfoMapper;
        private readonly ISpecialityMapper _specialityMapper;

        public CathedraMapper(
            ILecturerMapper lecturerMapper,
            ICommunicationInfoMapper communicationInfoMapper,
            ILocationInfoMapper locationInfoMapper,
            ISpecialityMapper specialityMapper
            )
        {
            _lecturerMapper = lecturerMapper;
            _communicationInfoMapper = communicationInfoMapper;
            _locationInfoMapper = locationInfoMapper;
            _specialityMapper = specialityMapper;
        }

        public Cathedra Map(Cathedras model)
        {
            var cathedra = new Cathedra
            {
                Id = model.Id,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                CathedraHead = _lecturerMapper.Map(model.CathedraHead),
                CathedraStaff = model.CathedrasStaff
                 .Select(x => _lecturerMapper.Map(x.Lecturer))
                 .ToList(),
                CommunicationInfo = _communicationInfoMapper.Map(model.CommunicationDetails),
                LocationDetails = _locationInfoMapper.Map(model.LocationDetails),
                Name = model.Name,
                Faculty = new Faculty
                {
                    CommunicationInfo = _communicationInfoMapper.Map(model.Faculty.CommunicationDetails),
                    CreatedAt = model.Faculty.CreatedAt,
                    Id = model.Faculty.Id,
                    Dean = _lecturerMapper.Map(model.Faculty.Dean),
                    Description = model.Faculty.Description,
                    FullName = model.Faculty.FullName,
                    ModifiedAt = model.Faculty.ModifiedAt,
                    ShortName = model.Faculty.ShortName,
                    Specialities = model.Faculty.Specialities.Select(x => _specialityMapper.Map(x)).ToList(),
                    WebSiteUrl = model.Faculty.WebSiteUrl       
                },
                Specialities = model.Specialities
                 .Select(x => _specialityMapper.Map(x))
                 .ToList()
            };

            return cathedra;
        }

        public Cathedras Map(Cathedra model)
        {
            var cathedraDbModel = new Cathedras
            {
                Id = model.Id,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                Name = model.Name,
                LocationDetails = _locationInfoMapper.Map(model.LocationDetails),
                CathedraHead = _lecturerMapper.Map(model.CathedraHead),
                CommunicationDetails = _communicationInfoMapper.Map(model.CommunicationInfo),
                Faculty = new Faculties
                {
                    CommunicationDetails = _communicationInfoMapper.Map(model.Faculty.CommunicationInfo),
                    CommunicationDetailsId = model.Faculty.CommunicationInfo.Id,
                    Dean = _lecturerMapper.Map(model.Faculty.Dean),
                    Id = model.Faculty.Id,
                    CreatedAt = model.Faculty.CreatedAt,
                    ModifiedAt = model.Faculty.ModifiedAt,
                    DeanId = model.Faculty.Dean.Id,
                    Description = model.Faculty.Description,
                    FullName = model.Faculty.FullName,
                    ShortName = model.Faculty.ShortName,
                    WebSiteUrl = model.Faculty.WebSiteUrl
                }  
            };

            return cathedraDbModel;
        }
    }
}
