﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class SpecializationMapper : ISpecializationMapper
    {

        public Specialization Map(Specializations model)
        {
            var specialization = new Specialization
            {
                Id = model.Id,
                Code = model.Code,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                Name = model.Name
            };

            return specialization;
        }

        public Specializations Map(Specialization model)
        {
            var specializationDbModel = new Specializations
            {
                Id = model.Id,
                Code = model.Code,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                Name = model.Name     
            };

            return specializationDbModel;
        }
    }
}
