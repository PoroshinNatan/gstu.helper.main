﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class StudyPeriodMapper : IStudyPeriodMapper
    {
        public StudyPeriod Map(StudyPeriods model)
        {
            var period = new StudyPeriod
            {
                Id = model.Id,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                Period = model.Period,
                FormType = (Enums.StudyFormType)model.FormType
            };

            return period;
        }

        public StudyPeriods Map(StudyPeriod model)
        {
            throw new NotImplementedException();
        }
    }
}
