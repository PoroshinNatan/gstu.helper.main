﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class FacultyMapper : IFacultyMapper
    {
        private readonly ICommunicationInfoMapper _communicationInfoMapper;
        private readonly ICathedraMapper _cathedraMapper;
        private readonly ILecturerMapper _lecturerMapper;
        private readonly ISpecialityMapper _specialityMapper;

        public FacultyMapper(
            ICommunicationInfoMapper communicationInfoMapper,
            ICathedraMapper cathedraMapper,
            ILecturerMapper lecturerMapper,
            ISpecialityMapper specialityMapper
            )
        {
            _communicationInfoMapper = communicationInfoMapper;
            _cathedraMapper = cathedraMapper;
            _lecturerMapper = lecturerMapper;
            _specialityMapper = specialityMapper;
        }

        public Faculty Map(Faculties model)
        {
            var faculty = new Faculty
            {
                CommunicationInfo = _communicationInfoMapper.Map(model.CommunicationDetails),
                Cathedras = model.Cathedras
                    .Select(x => _cathedraMapper.Map(x))
                    .ToList(),
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                Description = model.Description,
                FullName = model.FullName,
                Id = model.Id,
                ShortName = model.ShortName,
                Dean = _lecturerMapper.Map(model.Dean),
                WebSiteUrl = model.WebSiteUrl,
                Specialities = model.Specialities
                    .Select(x => _specialityMapper.Map(x))
                    .ToList()
            };

            return faculty;
        }

        //ПОСОМТРЕТЬ ВСЕ МАППЕРЫ В ЭТОМ МЕТОДЕ И ДОБАВИТЬ ИХ ФУНКЦИОНАЛ ЕСЛИ НЕ БУДЕТ!!
        public Faculties Map(Faculty model)
        {
            var facultyDbModel = new Faculties
            {
                Id = model.Id,
                CommunicationDetails = _communicationInfoMapper.Map(model.CommunicationInfo),
                Dean = _lecturerMapper.Map(model.Dean),
                Description = model.Description,
                FullName = model.FullName,
                ModifiedAt = model.ModifiedAt,
                CreatedAt = model.CreatedAt,
                WebSiteUrl = model.WebSiteUrl,
                ShortName = model.ShortName,
                CommunicationDetailsId = model.CommunicationInfo.Id,
                DeanId = model.Dean.Id     
            };

            return facultyDbModel;
        }
    }
}
