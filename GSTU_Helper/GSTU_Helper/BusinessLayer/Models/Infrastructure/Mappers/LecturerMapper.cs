﻿using GSTU_Helper.BusinessLayer.Models.Enums;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class LecturerMapper : ILecturerMapper
    {
        public Lecturer Map(Lecturers model)
        {
            var lecturer = new Lecturer
            {
                Id = model.Id,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                FirstName = model.FirstName,
                Gender = (Gender)model.Gender,
                LastName = model.LastName,
                ScientistGrade = (ScientistGrade)model.ScientistGrade
            };

            return lecturer;
        }

        public Lecturers Map(Lecturer model)
        {
            var lecturerDbModel = new Lecturers
            {
                Id = model.Id,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                FirstName = model.FirstName,
                Gender = (int?)model.Gender,
                LastName = model.LastName,
                ScientistGrade = (int?)model.ScientistGrade
            };

            return lecturerDbModel;
        }
    }
}
