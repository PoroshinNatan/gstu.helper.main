﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class LocationInfoMapper : ILocationInfoMapper
    {
        public LocationInfo Map(LocationDetails model)
        {
            var locationInfo = new LocationInfo
            {
                Id = model.Id,
                Address = model.Address,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                MailIndex = model.MailIndex,
                Room = model.Room,
                City = new City
                {
                    Id = model.City.Id,
                    Name = model.City.Name,
                    CreatedAt = model.City.CreatedAt,
                    ModifiedAt = model.City.ModifiedAt
                },
                Country = new Country
                {
                    Id = model.Country.Id,
                    Name = model.Country.Name,
                    CreatedAt = model.Country.CreatedAt,
                    ModifiedAt = model.Country.ModifiedAt
                }      
            };

            return locationInfo;
        }

        public LocationDetails Map(LocationInfo model)
        {
            var locationInfoDbModel = new LocationDetails
            {
                Id = model.Id,
                Address = model.Address,
                City = new Cities
                {
                    Id = model.City.Id,
                    CreatedAt = model.City.CreatedAt,
                    ModifiedAt = model.City.ModifiedAt,
                    Name = model.City.Name
                },
                Country = new Countries
                {
                    Id = model.Country.Id,
                    Name = model.Country.Name,
                    ModifiedAt = model.Country.ModifiedAt,
                    CreatedAt = model.CreatedAt
                },
                CreatedAt = model.CreatedAt,
                MailIndex = model.MailIndex,
                Room = model.Room,
                ModifiedAt = model.ModifiedAt   
            };

            return locationInfoDbModel;
        }
    }
}
