﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class CommandMapper : ICommandMapper
    {
        ICommandParameterMapper _commandParameterMapper;

        public CommandMapper(ICommandParameterMapper commandParameterMapper)
        {
            _commandParameterMapper = commandParameterMapper;
        }

        public Command Map(Commands model)
        {
            var command = new Command();

            command.Id = model.Id;
            command.CommandName = model.Name;
            command.CommandPattern = model.CommandPattern;
            command.Description = model.Description;
            command.CreatedAt = model.CreatedAt;
            command.ModifiedAt = model.ModifiedAt;
            command.IsActive = model.IsActive;
            command.CommandParameters = model.CommandArguments != null ? model.CommandArguments.Select(x =>
                _commandParameterMapper.Map(x)
            ).ToList() : null;

            return command; 
        }

        public Commands Map(Command model)
        {
            var command = new Commands();
            command.Id = model.Id;
            command.Name = model.CommandName;
            command.Description = model.Description;
            command.IsActive = model.IsActive;
            command.CommandPattern = model.CommandPattern;
            command.CommandArguments = model.CommandParameters != null ? model.CommandParameters.Select(x => new CommandArguments {
                ArgumentType = (int?)x.ArgumentType,
                ParameterName = x.ParameterName,
                CommandId = model.Id,
                Description = x.Description,
                Id = x.Id
            }).ToList() : null;

            return command;
        }
    }
}
