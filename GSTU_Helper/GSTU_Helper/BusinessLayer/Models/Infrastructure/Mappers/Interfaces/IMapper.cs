﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces
{
    public interface IMapper<TSource,TDest>
        where TSource : BaseModel
        where TDest : class
    {
        TSource Map(TDest model);
        TDest Map(TSource model);
    }
}
