﻿using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces
{
    public interface ILocationInfoMapper : IMapper<LocationInfo, LocationDetails>
    {
    }
}
