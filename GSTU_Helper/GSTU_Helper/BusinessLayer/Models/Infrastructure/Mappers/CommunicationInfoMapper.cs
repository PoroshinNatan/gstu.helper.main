﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class CommunicationInfoMapper : ICommunicationInfoMapper
    {

        public CommunicationInfo Map(CommunicationDetails model)
        {
            var communication = new CommunicationInfo
            {
                Id = model.Id,
                CreatedAt = model.CreatedAt,
                ModifiedAt = model.ModifiedAt,
                Email = model.Email,
                Phone = model.Phone
            };

            return communication;
        }

        public CommunicationDetails Map(CommunicationInfo model)
        {
            var dbModel = new CommunicationDetails
            {
                Id = model.Id,
                CreatedAt = model.CreatedAt,
                Email = model.Email,
                ModifiedAt = model.ModifiedAt,
                Phone = model.Phone
            };

            return dbModel;
        }
    }
}
