﻿using GSTU_Helper.BusinessLayer.Models.Enums;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers
{
    public class CommandParameterMapper : ICommandParameterMapper
    {
        public CommandParameter Map(CommandArguments model)
        {
            var command = new CommandParameter {
                Id = model.Id,
                ArgumentType = model.ArgumentType.HasValue ? (ArgumentType)model.ArgumentType : ArgumentType.None,
                ParameterName = model.ParameterName,
                Description = model.Description
            };

            return command;
        }

        public CommandArguments Map(CommandParameter model)
        {
            var commandDbModel = new CommandArguments
            {
                ArgumentType = (int?)model.ArgumentType,
                Description = model.Description,
                Id = model.Id,
                ParameterName = model.ParameterName
            };

            return commandDbModel;
        }
    }
}
