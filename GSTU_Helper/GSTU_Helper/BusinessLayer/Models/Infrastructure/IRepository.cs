﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure
{
    public interface IRepository
    {
        ICommandService CommandService { get; set; }

        ICathedraService CathedraService { get; set; }
    }
}
