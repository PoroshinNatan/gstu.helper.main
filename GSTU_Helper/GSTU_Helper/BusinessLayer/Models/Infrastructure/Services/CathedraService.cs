﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Services
{
    public class CathedraService : ModelService, ICathedraService
    {
        private readonly ICathedraMapper _cathedraMapper;

        public CathedraService(
            GstuHelperDbContext context,
            ICathedraMapper cathedraMapper
            )
            : base(context)
        {
            _cathedraMapper = cathedraMapper;
        }

        public void Add(Cathedra model)
        {
            GstuHelperDbContext.Cathedras.Add(_cathedraMapper.Map(model));
        }

        public void Delete(Cathedra model)
        {
            var cathedra = GstuHelperDbContext.Cathedras.Find(model.Id);

            if (cathedra != null)
            {
                GstuHelperDbContext.Cathedras.Remove(_cathedraMapper.Map(model));
            }
        }

        public Cathedra Get(int id)
        {
            return _cathedraMapper.Map(GstuHelperDbContext.Cathedras
                .Include(x => x.CommunicationDetails)
                .Include(x => x.Faculty.Dean)
                .Include(x => x.LocationDetails)
                .ThenInclude(x => x.City)
                .Include(x => x.LocationDetails)
                .ThenInclude(x => x.Country)
                .Include(x => x.CathedraHead)
                .FirstOrDefault(x => x.Id == id));
        }

        public IList<Cathedra> GetAll()
        {
            return GstuHelperDbContext.Cathedras
                .Include(x => x.CommunicationDetails)
                .Include(x => x.Faculty.Dean)
                .Include(x => x.LocationDetails)
                .ThenInclude(x => x.City)
                .Include(x => x.LocationDetails)
                .ThenInclude(x => x.Country)
                .Include(x => x.CathedraHead)
                .Select(x => _cathedraMapper.Map(x))
                .ToList();
        }

        public void Update(Cathedra newModel)
        {
            GstuHelperDbContext.Cathedras.Update(_cathedraMapper.Map(newModel));
        }
    }
}
