﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces
{
    public interface ICathedraService : IService<Cathedra>
    {
    }
}
