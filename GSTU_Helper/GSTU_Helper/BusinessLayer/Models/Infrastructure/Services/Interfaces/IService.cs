﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces
{
    public interface IService<T>
        where T : BaseModel
    {
        T Get(int id);
        IList<T> GetAll();
        void Add(T model);
        void Update(T newModel);
        void Delete(T model);
    }
}
