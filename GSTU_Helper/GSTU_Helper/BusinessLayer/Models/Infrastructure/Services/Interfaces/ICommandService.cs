﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces
{
    public interface ICommandService : IService<Command>
    {
        void DeactivateCommand(int id);

        void ActivateCommand(int id);
    }
}
