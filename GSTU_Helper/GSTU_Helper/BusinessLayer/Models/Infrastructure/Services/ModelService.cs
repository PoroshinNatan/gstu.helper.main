﻿using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Services
{
    public class ModelService
    {
        protected GstuHelperDbContext GstuHelperDbContext { get; private set; }

        public ModelService(GstuHelperDbContext context)
        {
            GstuHelperDbContext = context;
        }
    }
}
