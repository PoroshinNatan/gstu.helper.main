﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Services
{
    public class FacultyService : ModelService, IFacultyService
    {
        private readonly IFacultyMapper _facultyMapper;

        public FacultyService(
            GstuHelperDbContext context,
            IFacultyMapper facultyMapper
            )
            : base(context)
        {
            _facultyMapper = facultyMapper;
        }

        public void Add(Faculty model)
        {
            GstuHelperDbContext.Faculties.Add(_facultyMapper.Map(model));
        }

        public void Delete(Faculty model)
        {
            GstuHelperDbContext.Faculties.Remove(_facultyMapper.Map(model));
        }

        public Faculty Get(int id)
        {
            throw new NotImplementedException();
        }

        public IList<Faculty> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Update(Faculty newModel)
        {
            throw new NotImplementedException();
        }
    }
}
