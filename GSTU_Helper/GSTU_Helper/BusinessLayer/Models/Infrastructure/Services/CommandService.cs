﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GSTU_Helper.BusinessLayer.Models.Infrastructure.Services
{
    public class CommandService : ModelService, ICommandService
    {
        private readonly ICommandMapper _commandMapper;

        public CommandService(GstuHelperDbContext context,
            ICommandMapper commandMapper) 
            : base(context)
        {
            _commandMapper = commandMapper;
        }

        public void ActivateCommand(int id)
        {
            var entity = GstuHelperDbContext.Commands.Find(id);

            entity.ModifiedAt = DateTime.Now;
            entity.IsActive = true;

            GstuHelperDbContext.Commands.Update(entity);

            GstuHelperDbContext.SaveChanges();
        }

        public void Add(Command model)
        {
            var entity = _commandMapper.Map(model);

            if (!entity.IsActive.HasValue)
            {
                entity.IsActive = true;
            }

            entity.ModifiedAt = DateTime.Now;
            entity.CreatedAt = DateTime.Now;

            GstuHelperDbContext.Commands.Add(entity);

            GstuHelperDbContext.SaveChanges();
        }

        public void DeactivateCommand(int id)
        {
            var entity = GstuHelperDbContext.Commands.Find(id);

            entity.ModifiedAt = DateTime.Now;
            entity.IsActive = false;

            GstuHelperDbContext.Commands.Update(entity);

            GstuHelperDbContext.SaveChanges();
        }

        public void Delete(Command model)
        {
            var entity = GstuHelperDbContext.Commands.Find(model.Id);

            if(entity != null)
            {
                GstuHelperDbContext.Commands.Remove(entity);
                GstuHelperDbContext.SaveChanges();
            }
        }

        public Command Get(int id)
        {
            return _commandMapper.Map(GstuHelperDbContext
                .Commands
                .Include(x => x.CommandArguments)
                .ToList()
                .FirstOrDefault(x => x.Id == id));
        }

        public IList<Command> GetAll()
        {
            return GstuHelperDbContext
                .Commands
                .Include(x => x.CommandArguments)
                .Select(x => _commandMapper.Map(x))
                .ToList();
        }

        public void Update(Command newModel)
        {
            var entity = _commandMapper.Map(newModel);

            entity.ModifiedAt = DateTime.Now;

            GstuHelperDbContext.Commands.Update(entity);

            GstuHelperDbContext.SaveChanges();
        }
    }
}
