﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class Discipline : BaseModel
    {
        public string Description { get; set; }
        public float? Difficulty { get; set; }
    }
}
