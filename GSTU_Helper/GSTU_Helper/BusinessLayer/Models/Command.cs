﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class Command : BaseModel
    {
        public string CommandName { get; set; }

        public IList<CommandParameter> CommandParameters { get; set; }

        public string Description { get; set; }

        public string CommandPattern { get; set; }

        public bool? IsActive { get; set; }

        public string GetCommandDescription()
        {
            var description = string.Format("Имя команды: {0};{1}", CommandName, Environment.NewLine);
            description += string.Format("Паттерн: {0};{1}", CommandPattern, Environment.NewLine);
            description += string.Format("Описание: {0};{1}", Description, Environment.NewLine);

            if (CommandParameters != null)
            {
                description += string.Format("Параметры: {0}", Environment.NewLine);
                if (CommandParameters.Count > 0)
                {
                    foreach (var param in CommandParameters)
                    {
                        description += string.Format("{0}{1} - {2};{3}", '\t', param.ParameterName, param.Description, Environment.NewLine);
                    }
                }
                else
                {
                    description += "Параметры для этой команды еще не добавлены.";
                }
            }
            else
            {
                description += "Параметров для этой команды нет.";
            }

            return description;
        }
    }
}
