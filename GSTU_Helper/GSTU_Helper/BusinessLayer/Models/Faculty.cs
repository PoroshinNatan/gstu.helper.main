﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class Faculty : BaseModel
    {
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public string WebSiteUrl { get; set; }
        public CommunicationInfo CommunicationInfo { get; set; }
        public Lecturer Dean { get; set; }
        public IList<Cathedra> Cathedras { get; set; }
        public IList<Speciality> Specialities { get; set; }
    }
}
