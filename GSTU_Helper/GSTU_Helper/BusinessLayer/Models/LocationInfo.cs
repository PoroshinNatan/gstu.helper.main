﻿using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.BusinessLayer.Models
{
    public class LocationInfo : BaseModel
    {
        public string Address { get; set; }
        public string MailIndex { get; set; }
        public string Room { get; set; }
        public City City { get; set; }
        public Country Country { get; set; }
    }
}
