﻿using GSTU_Helper.BusinessLayer.ChatBot.Services;
using GSTU_Helper.BusinessLayer.Models;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces;
using GSTU_Helper.DataLayer.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.Extensions
{
    public static class ServiceProviderExtensions
    {

        public static void AddBotServices(this IServiceCollection services)
        {
            services.AddSingleton<IBotService, BotService>()
                .AddTransient<IDialogflowService, DialogflowService>()
                .AddTransient<BusinessLayer.ChatBot.Services.ICommandService, BusinessLayer.ChatBot.Services.CommandService>()
                .AddTransient<ISqlSyntaxTranslatorService, SqlSyntaxTranslatorService>();
        }

        public static void AddModelServices(this IServiceCollection services)
        {
            services.AddTransient<BusinessLayer.Models.Infrastructure.Services.Interfaces.ICommandService,
                BusinessLayer.Models.Infrastructure.Services.CommandService>(x =>
                {
                    GstuHelperDbContext context;
                    var serviceScope = x.CreateScope();
                    context = serviceScope.ServiceProvider.GetService<GstuHelperDbContext>();

                    return new BusinessLayer.Models.Infrastructure.Services.CommandService(context, x.GetService<ICommandMapper>());
                })
                .AddTransient<ICathedraService, CathedraService>(x =>
                {
                    GstuHelperDbContext context;
                    var serviceScope = x.CreateScope();
                    context = serviceScope.ServiceProvider.GetService<GstuHelperDbContext>();

                    return new CathedraService(context, x.GetService<ICathedraMapper>());
                })
                .AddTransient<IFacultyService, FacultyService>(x =>
                {
                    GstuHelperDbContext context;
                    var serviceScope = x.CreateScope();
                    context = serviceScope.ServiceProvider.GetService<GstuHelperDbContext>();

                    return new FacultyService(context, x.GetService<IFacultyMapper>());
                })
                .AddTransient<IRepository, Repository>();
                
                
        }

        public static void AddMappers(this IServiceCollection services)
        {
            services.AddTransient<ICommandMapper, CommandMapper>()
                .AddTransient<ICommandParameterMapper, CommandParameterMapper>()
                .AddTransient<ILocationInfoMapper, LocationInfoMapper>()
                .AddTransient<ILecturerMapper, LecturerMapper>()
                .AddTransient<IFacultyMapper, FacultyMapper>()
                .AddTransient<IDisciplineMapper, DisciplineMapper>()
                .AddTransient<ISpecialityMapper, SpecialityMapper>()
                .AddTransient<ISpecializationMapper, SpecializationMapper>()
                .AddTransient<IStudyPeriodMapper, StudyPeriodMapper>()
                .AddTransient<ICommunicationInfoMapper, CommunicationInfoMapper>()
                .AddTransient<ICommandParameterMapper, CommandParameterMapper>()
                .AddTransient<ICathedraMapper, CathedraMapper>();
        }
    }
}
