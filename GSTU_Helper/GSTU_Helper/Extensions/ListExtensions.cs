﻿using GSTU_Helper.ViewModels.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.Extensions
{
    public static class ListExtensions
    {
        public static PaginModel ToPaginModel<T>(this List<T> list, int displayCount, int currentPage)
        {
            return new PaginModel(displayCount, currentPage, Convert.ToInt32(Math.Ceiling((double)list.Count/displayCount)));
        }

        public static IEnumerable<T> GetItemsByPaginModel<T>(this List<T> list, PaginModel paginModel)
        {
            return list.Skip(paginModel.DisplayCount * (paginModel.CurrentPage - 1)).Take(paginModel.DisplayCount);
        }
    }
}
