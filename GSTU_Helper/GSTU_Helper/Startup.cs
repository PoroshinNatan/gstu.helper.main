using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection; 
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using GSTU_Helper.DataLayer.DataModels;
using GSTU_Helper.Configurations;
using GSTU_Helper.BusinessLayer.ChatBot.Services;
using GSTU_Helper.BusinessLayer.Models.Infrastructure;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers.Interfaces;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Mappers;
using GSTU_Helper.BusinessLayer.Models;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services.Interfaces;
using GSTU_Helper.BusinessLayer.Models.Infrastructure.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using GSTU_Helper.Extensions;


namespace GSTU_Helper
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLocalization(option => {
                option.ResourcesPath = "Resources";
            });

            services.AddMvc()
                .AddViewLocalization();

            services.AddRouting();

            services.AddDbContext<GstuHelperDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("Deploy"));
            });

            services
                .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
            {
                options.LoginPath = "/Account/Login";
                options.LogoutPath = "/Account/Logoff";
            });

            services.AddIdentity<IdentityUser, IdentityRole>(o =>
            {
                o.Password.RequireDigit = false;
                o.Password.RequiredLength = 5;
                o.Password.RequireLowercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequireUppercase = false;
                o.Password.RequiredUniqueChars = 0;
                o.SignIn.RequireConfirmedEmail = false;
                o.SignIn.RequireConfirmedPhoneNumber = false;
                o.User.RequireUniqueEmail = false;
            })
                .AddEntityFrameworkStores<GstuHelperDbContext>()
                .AddDefaultTokenProviders();

            services.AddMappers();
            services.AddModelServices();
            services.AddBotServices();


            services.Configure<BotConfigurations>(Configuration.GetSection("BotConfigurations"));
            services.Configure<DialogflowConfigurations>(Configuration.GetSection("DialogflowConfigurations"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            var supportedCultures = new CultureInfo[] {
                new CultureInfo("ru")
            };

            app.UseRequestLocalization(new RequestLocalizationOptions {
                DefaultRequestCulture = new RequestCulture(supportedCultures[0]),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvcWithDefaultRoute();

            app.ApplicationServices.GetService<IBotService>();
        }
    }
}
