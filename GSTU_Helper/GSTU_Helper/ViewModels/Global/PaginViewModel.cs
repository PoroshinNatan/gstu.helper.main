﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.ViewModels.Global
{
    public class PaginModel
    {
        public PaginModel()
        {
            DisplayCount = 8;
            CurrentPage = 1;
            TotalCount = 1;
        }

        public PaginModel(int displayCount, int currentPage, int totalCount)
        { 
            DisplayCount = displayCount;
            CurrentPage = currentPage;
            TotalCount = totalCount;
        }

        public int DisplayCount { get; set; }

        public int CurrentPage { get; set; }

        public int TotalCount { get; set; }
    }
}
