﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.ViewModels.Authorization
{
    public class LoginViewModel
    {
        [Required]
        [StringLength(50)]
        public string Login { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string BackUrl { get; set; }
    }
}
