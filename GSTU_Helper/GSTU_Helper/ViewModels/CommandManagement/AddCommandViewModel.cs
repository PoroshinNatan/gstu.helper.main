﻿using GSTU_Helper.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.ViewModels.CommandManagement
{
    public class AddCommandViewModel
    {
        Command Command { get; set; }
    }
}
