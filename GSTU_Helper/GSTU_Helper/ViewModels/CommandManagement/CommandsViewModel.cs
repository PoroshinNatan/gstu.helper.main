﻿using GSTU_Helper.BusinessLayer.Models;
using GSTU_Helper.ViewModels.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.ViewModels.CommandManagement
{
    public class CommandsViewModel
    {
        public List<Command> Commands { get; set; }

        public PaginModel Pagin { get; set; }
    }
}
