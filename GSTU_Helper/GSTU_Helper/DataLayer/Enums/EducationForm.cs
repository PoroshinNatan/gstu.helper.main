﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSTU_Helper.DataLayer.Enums
{
    public enum EducationForm
    {
        Daytime,
        Extramural,
        ExtramuralShortened
    }
}
