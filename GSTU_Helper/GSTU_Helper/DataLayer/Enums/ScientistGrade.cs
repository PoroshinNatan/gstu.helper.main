﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSTU_Helper.DataLayer.Enums
{
    public enum ScientistGrade
    {
        //Аспирант
        GraduateStudent,
        //Ассистент
        Assistant,
        //Старший преподаватель
        SeniorLecturer,
        //Профессор
        Professor,
        //Доцент
        AssociateProfessor,
        //Стажер
        Trainee
    }
}
