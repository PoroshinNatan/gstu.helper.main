﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class SpecialityStudyPeriods
    {
        public int Id { get; set; }
        public int? StudyPeriodId { get; set; }
        public int? SpecialityId { get; set; }

        public Specialities Speciality { get; set; }
        public StudyPeriods StudyPeriod { get; set; }
    }
}
