﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class CommunicationDetails
    {
        public CommunicationDetails()
        {
            Cathedras = new HashSet<Cathedras>();
            Faculties = new HashSet<Faculties>();
        }

        public int Id { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public ICollection<Cathedras> Cathedras { get; set; }
        public ICollection<Faculties> Faculties { get; set; }
    }
}
