﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class Specialities
    {
        public Specialities()
        {
            SpecialityActivityFields = new HashSet<SpecialityActivityFields>();
            SpecialityStudyDisciplines = new HashSet<SpecialityStudyDisciplines>();
            SpecialityStudyPeriods = new HashSet<SpecialityStudyPeriods>();
            Specializations = new HashSet<Specializations>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Qualification { get; set; }
        public int? FacultyId { get; set; }
        public int? CathedraId { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public Cathedras Cathedra { get; set; }
        public Faculties Faculty { get; set; }
        public ICollection<SpecialityActivityFields> SpecialityActivityFields { get; set; }
        public ICollection<SpecialityStudyDisciplines> SpecialityStudyDisciplines { get; set; }
        public ICollection<SpecialityStudyPeriods> SpecialityStudyPeriods { get; set; }
        public ICollection<Specializations> Specializations { get; set; }
    }
}
