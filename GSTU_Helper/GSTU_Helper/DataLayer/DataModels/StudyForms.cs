﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class StudyForms
    {
        public int Id { get; set; }
        public int? FormType { get; set; }
        public string FormName { get; set; }
        public int? PaymentType { get; set; }
        public string PaymentTypeName { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
    }
}
