﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class SpecialityStudyDisciplines
    {
        public int Id { get; set; }
        public int? StudyDisciplineId { get; set; }
        public int? SpecialityId { get; set; }

        public Specialities Speciality { get; set; }
        public StudyDisciplines StudyDiscipline { get; set; }
    }
}
