﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class SpecialityActivityFields
    {
        public int Id { get; set; }
        public int? ActivityFieldId { get; set; }
        public int? SpecialityId { get; set; }

        public ActivityFields ActivityField { get; set; }
        public Specialities Speciality { get; set; }
    }
}
