﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class Faculties
    {
        public Faculties()
        {
            Cathedras = new HashSet<Cathedras>();
            Specialities = new HashSet<Specialities>();
        }

        public int Id { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public int? CommunicationDetailsId { get; set; }
        public string WebSiteUrl { get; set; }
        public int? DeanId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public CommunicationDetails CommunicationDetails { get; set; }
        public Lecturers Dean { get; set; }
        public ICollection<Cathedras> Cathedras { get; set; }
        public ICollection<Specialities> Specialities { get; set; }
    }
}
