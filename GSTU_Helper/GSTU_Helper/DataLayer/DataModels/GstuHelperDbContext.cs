﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class GstuHelperDbContext : IdentityDbContext
    {
        private readonly IConfiguration _configs;

        public virtual DbSet<ActivityFields> ActivityFields { get; set; }
        public virtual DbSet<Cathedras> Cathedras { get; set; }
        public virtual DbSet<CathedrasStaff> CathedrasStaff { get; set; }
        public virtual DbSet<Cities> Cities { get; set; }
        public virtual DbSet<CommunicationDetails> CommunicationDetails { get; set; }
        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<Faculties> Faculties { get; set; }
        public virtual DbSet<Lecturers> Lecturers { get; set; }
        public virtual DbSet<LocationDetails> LocationDetails { get; set; }
        public virtual DbSet<Specialities> Specialities { get; set; }
        public virtual DbSet<SpecialityActivityFields> SpecialityActivityFields { get; set; }
        public virtual DbSet<SpecialityStudyDisciplines> SpecialityStudyDisciplines { get; set; }
        public virtual DbSet<SpecialityStudyPeriods> SpecialityStudyPeriods { get; set; }
        public virtual DbSet<Specializations> Specializations { get; set; }
        public virtual DbSet<StudyDisciplines> StudyDisciplines { get; set; }
        public virtual DbSet<StudyForms> StudyForms { get; set; }
        public virtual DbSet<StudyPeriods> StudyPeriods { get; set; }
        public virtual DbSet<Commands> Commands { get; set; }
        public virtual DbSet<CommandArguments> CommandArguments { get; set; }

        public GstuHelperDbContext(DbContextOptions<GstuHelperDbContext> options, IConfiguration configs) 
            : base(options)
        {
            _configs = configs;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(_configs.GetConnectionString("Deploy"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ActivityFields>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<Cathedras>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.CathedraHead)
                    .WithMany(p => p.Cathedras)
                    .HasForeignKey(d => d.CathedraHeadId)
                    .HasConstraintName("FK__Cathedras__Cathe__74AE54BC");

                entity.HasOne(d => d.CommunicationDetails)
                    .WithMany(p => p.Cathedras)
                    .HasForeignKey(d => d.CommunicationDetailsId)
                    .HasConstraintName("FK__Cathedras__Commu__73BA3083");

                entity.HasOne(d => d.Faculty)
                    .WithMany(p => p.Cathedras)
                    .HasForeignKey(d => d.FacultyId)
                    .HasConstraintName("FK__Cathedras__Facul__75A278F5");
            });

            modelBuilder.Entity<CathedrasStaff>(entity =>
            {
                entity.HasOne(d => d.Cathedra)
                    .WithMany(p => p.CathedrasStaff)
                    .HasForeignKey(d => d.CathedraId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Cathedras__Cathe__787EE5A0");

                entity.HasOne(d => d.Lecturer)
                    .WithMany(p => p.CathedrasStaff)
                    .HasForeignKey(d => d.LecturerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Cathedras__Lectu__797309D9");
            });

            modelBuilder.Entity<Cities>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CommunicationDetails>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.Phone).HasMaxLength(20);
            });

            modelBuilder.Entity<Countries>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Faculties>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.FullName).HasMaxLength(100);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.ShortName).HasMaxLength(10);

                entity.HasOne(d => d.CommunicationDetails)
                    .WithMany(p => p.Faculties)
                    .HasForeignKey(d => d.CommunicationDetailsId)
                    .HasConstraintName("FK__Faculties__Commu__6FE99F9F");

                entity.HasOne(d => d.Dean)
                    .WithMany(p => p.Faculties)
                    .HasForeignKey(d => d.DeanId)
                    .HasConstraintName("FK__Faculties__DeanI__70DDC3D8");
            });

            modelBuilder.Entity<Lecturers>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<LocationDetails>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.MailIndex).HasMaxLength(20);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.Room).HasMaxLength(20);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.LocationDetails)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK__LocationD__CityI__60A75C0F");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.LocationDetails)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK__LocationD__Count__619B8048");
            });

            modelBuilder.Entity<Specialities>(entity =>
            {
                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.HasOne(d => d.Cathedra)
                    .WithMany(p => p.Specialities)
                    .HasForeignKey(d => d.CathedraId)
                    .HasConstraintName("FK__Specialit__Cathe__02FC7413");

                entity.HasOne(d => d.Faculty)
                    .WithMany(p => p.Specialities)
                    .HasForeignKey(d => d.FacultyId)
                    .HasConstraintName("FK__Specialit__Facul__02084FDA");
            });

            modelBuilder.Entity<SpecialityActivityFields>(entity =>
            {
                entity.HasOne(d => d.ActivityField)
                    .WithMany(p => p.SpecialityActivityFields)
                    .HasForeignKey(d => d.ActivityFieldId)
                    .HasConstraintName("FK__Specialit__Activ__08B54D69");

                entity.HasOne(d => d.Speciality)
                    .WithMany(p => p.SpecialityActivityFields)
                    .HasForeignKey(d => d.SpecialityId)
                    .HasConstraintName("FK__Specialit__Speci__09A971A2");
            });

            modelBuilder.Entity<SpecialityStudyDisciplines>(entity =>
            {
                entity.HasOne(d => d.Speciality)
                    .WithMany(p => p.SpecialityStudyDisciplines)
                    .HasForeignKey(d => d.SpecialityId)
                    .HasConstraintName("FK__Specialit__Speci__0D7A0286");

                entity.HasOne(d => d.StudyDiscipline)
                    .WithMany(p => p.SpecialityStudyDisciplines)
                    .HasForeignKey(d => d.StudyDisciplineId)
                    .HasConstraintName("FK__Specialit__Study__0C85DE4D");
            });

            modelBuilder.Entity<SpecialityStudyPeriods>(entity =>
            {
                entity.HasOne(d => d.Speciality)
                    .WithMany(p => p.SpecialityStudyPeriods)
                    .HasForeignKey(d => d.SpecialityId)
                    .HasConstraintName("FK__Specialit__Speci__1332DBDC");

                entity.HasOne(d => d.StudyPeriod)
                    .WithMany(p => p.SpecialityStudyPeriods)
                    .HasForeignKey(d => d.StudyPeriodId)
                    .HasConstraintName("FK__Specialit__Study__123EB7A3");
            });

            modelBuilder.Entity<Specializations>(entity =>
            {
                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.HasOne(d => d.Speciality)
                    .WithMany(p => p.Specializations)
                    .HasForeignKey(d => d.SpecialityId)
                    .HasConstraintName("FK__Specializ__Speci__05D8E0BE");
            });

            modelBuilder.Entity<StudyDisciplines>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<StudyForms>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.FormName).HasMaxLength(100);

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.PaymentTypeName).HasMaxLength(100);
            });

            modelBuilder.Entity<StudyPeriods>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");
            });
        }
    }
}
