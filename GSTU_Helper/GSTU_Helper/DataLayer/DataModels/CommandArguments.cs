﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class CommandArguments
    {
        public int Id { get; set; }
        public string ParameterName { get; set; }
        public int? ArgumentType { get; set; }
        public int? CommandId { get; set; }
        public string Description { get; set; }

        public Commands Command { get; set; }
    }
}
