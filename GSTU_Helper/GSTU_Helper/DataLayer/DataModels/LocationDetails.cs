﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class LocationDetails
    {
        public LocationDetails()
        {
            Cathedras = new HashSet<Cathedras>();
        }

        public int Id { get; set; }
        public int? CityId { get; set; }
        public int? CountryId { get; set; }
        public string Address { get; set; }
        public string MailIndex { get; set; }
        public string Room { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public Cities City { get; set; }
        public Countries Country { get; set; }

        public ICollection<Cathedras> Cathedras { get; set; }
    }
}
