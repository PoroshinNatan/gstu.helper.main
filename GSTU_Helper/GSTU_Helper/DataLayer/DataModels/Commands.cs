﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class Commands
    {
        public Commands()
        {
            CommandArguments = new HashSet<CommandArguments>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string CommandPattern { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public ICollection<CommandArguments> CommandArguments { get; set; }
    }
}
