﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class StudyDisciplines
    {
        public StudyDisciplines()
        {
            SpecialityStudyDisciplines = new HashSet<SpecialityStudyDisciplines>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public float? Difficulty { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public ICollection<SpecialityStudyDisciplines> SpecialityStudyDisciplines { get; set; }
    }
}
