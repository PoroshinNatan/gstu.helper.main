﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class CathedrasStaff
    {
        public int Id { get; set; }
        public int CathedraId { get; set; }
        public int LecturerId { get; set; }

        public Cathedras Cathedra { get; set; }
        public Lecturers Lecturer { get; set; }
    }
}
