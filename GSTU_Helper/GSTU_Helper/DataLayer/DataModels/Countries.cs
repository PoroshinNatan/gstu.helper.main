﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class Countries
    {
        public Countries()
        {
            LocationDetails = new HashSet<LocationDetails>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public ICollection<LocationDetails> LocationDetails { get; set; }
    }
}
