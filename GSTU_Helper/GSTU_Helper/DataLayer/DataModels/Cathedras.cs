﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class Cathedras
    {
        public Cathedras()
        {
            CathedrasStaff = new HashSet<CathedrasStaff>();
            Specialities = new HashSet<Specialities>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? CommunicationDetailsId { get; set; }
        public int? LocationDetailsId { get; set; }
        public int? CathedraHeadId { get; set; }
        public int? FacultyId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public Lecturers CathedraHead { get; set; }
        public CommunicationDetails CommunicationDetails { get; set; }
        public Faculties Faculty { get; set; }
        public LocationDetails LocationDetails { get; set; }
        public ICollection<CathedrasStaff> CathedrasStaff { get; set; }
        public ICollection<Specialities> Specialities { get; set; }
    }
}
