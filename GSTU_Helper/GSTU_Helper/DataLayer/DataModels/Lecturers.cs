﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class Lecturers
    {
        public Lecturers()
        {
            Cathedras = new HashSet<Cathedras>();
            CathedrasStaff = new HashSet<CathedrasStaff>();
            Faculties = new HashSet<Faculties>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? Gender { get; set; }
        public int? ScientistGrade { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public ICollection<Cathedras> Cathedras { get; set; }
        public ICollection<CathedrasStaff> CathedrasStaff { get; set; }
        public ICollection<Faculties> Faculties { get; set; }
    }
}
