﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class ActivityFields
    {
        public ActivityFields()
        {
            SpecialityActivityFields = new HashSet<SpecialityActivityFields>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public ICollection<SpecialityActivityFields> SpecialityActivityFields { get; set; }
    }
}
