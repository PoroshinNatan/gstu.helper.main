﻿using System;
using System.Collections.Generic;

namespace GSTU_Helper.DataLayer.DataModels
{
    public partial class StudyPeriods
    {
        public StudyPeriods()
        {
            SpecialityStudyPeriods = new HashSet<SpecialityStudyPeriods>();
        }

        public int Id { get; set; }
        public int? FormType { get; set; }
        public string FormName { get; set; }
        public int? Period { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public ICollection<SpecialityStudyPeriods> SpecialityStudyPeriods { get; set; }
    }
}
