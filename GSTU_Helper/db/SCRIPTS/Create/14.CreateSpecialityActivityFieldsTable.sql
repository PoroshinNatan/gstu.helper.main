USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'SpecialityActivityFields', N'U') IS NULL )
BEGIN
	CREATE TABLE [SpecialityActivityFields](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[ActivityFieldId] INT,
		[SpecialityId] INT,
		FOREIGN KEY(ActivityFieldId) REFERENCES [ActivityFields](Id),
		FOREIGN KEY(SpecialityId) REFERENCES [Specialities](Id)
	)
END

GO