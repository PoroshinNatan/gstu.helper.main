USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'StudyDisciplines', N'U') IS NULL )
BEGIN
	CREATE TABLE [StudyDisciplines](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[Description] NTEXT,
		[Difficulty] INT,
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME
	)
END

GO