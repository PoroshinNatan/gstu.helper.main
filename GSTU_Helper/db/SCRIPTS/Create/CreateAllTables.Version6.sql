USE [GstuHelperDb]
GO

/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[ActivityFields]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ActivityFields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[ModifiedAt] [datetime] NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_ActivityFields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[UserName] [nvarchar](256) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Cathedras]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cathedras](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CathedraHeadId] [int] NULL,
	[CommunicationDetailsId] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[FacultyId] [int] NULL,
	[LocationDetailsId] [int] NULL,
	[ModifiedAt] [datetime] NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Cathedras] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[CathedrasStaff]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CathedrasStaff](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CathedraId] [int] NOT NULL,
	[LecturerId] [int] NOT NULL,
 CONSTRAINT [PK_CathedrasStaff] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Cities]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[ModifiedAt] [datetime] NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[CommandArguments]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CommandArguments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommandId] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[ParameterName] [nvarchar](max) NULL,
	[ArgumentType] [int] NULL,
 CONSTRAINT [PK_CommandArguments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Commands]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Commands](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommandPattern] [nvarchar](max) NULL,
	[CreatedAt] [datetime2](7) NULL,
	[Description] [nvarchar](max) NULL,
	[ModifiedAt] [datetime2](7) NULL,
	[Name] [nvarchar](max) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Commands] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[CommunicationDetails]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CommunicationDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[Email] [nvarchar](50) NULL,
	[ModifiedAt] [datetime] NULL,
	[Phone] [nvarchar](20) NULL,
 CONSTRAINT [PK_CommunicationDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Countries]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Countries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[ModifiedAt] [datetime] NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Faculties]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Faculties](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CommunicationDetailsId] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[DeanId] [int] NULL,
	[Description] [ntext] NULL,
	[FullName] [nvarchar](100) NULL,
	[ModifiedAt] [datetime] NULL,
	[ShortName] [nvarchar](10) NULL,
	[WebSiteUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_Faculties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Lecturers]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Lecturers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[FirstName] [nvarchar](max) NULL,
	[Gender] [int] NULL,
	[LastName] [nvarchar](max) NULL,
	[ModifiedAt] [datetime] NULL,
	[ScientistGrade] [int] NULL,
 CONSTRAINT [PK_Lecturers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[LocationDetails]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LocationDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Address] [nvarchar](max) NULL,
	[CityId] [int] NULL,
	[CountryId] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[MailIndex] [nvarchar](20) NULL,
	[ModifiedAt] [datetime] NULL,
	[Room] [nvarchar](20) NULL,
 CONSTRAINT [PK_LocationDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Specialities]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Specialities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CathedraId] [int] NULL,
	[Code] [nvarchar](50) NULL,
	[CreatedAt] [datetime] NULL,
	[Description] [ntext] NULL,
	[FacultyId] [int] NULL,
	[ModifiedAt] [datetime] NULL,
	[Name] [nvarchar](max) NULL,
	[Qualification] [nvarchar](max) NULL,
 CONSTRAINT [PK_Specialities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[SpecialityActivityFields]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SpecialityActivityFields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ActivityFieldId] [int] NULL,
	[SpecialityId] [int] NULL,
 CONSTRAINT [PK_SpecialityActivityFields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[SpecialityStudyDisciplines]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SpecialityStudyDisciplines](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpecialityId] [int] NULL,
	[StudyDisciplineId] [int] NULL,
 CONSTRAINT [PK_SpecialityStudyDisciplines] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[SpecialityStudyPeriods]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SpecialityStudyPeriods](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SpecialityId] [int] NULL,
	[StudyPeriodId] [int] NULL,
 CONSTRAINT [PK_SpecialityStudyPeriods] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Specializations]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Specializations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[CreatedAt] [datetime] NULL,
	[ModifiedAt] [datetime] NULL,
	[Name] [nvarchar](100) NULL,
	[SpecialityId] [int] NULL,
 CONSTRAINT [PK_Specializations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[StudyDisciplines]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StudyDisciplines](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[Description] [ntext] NULL,
	[Difficulty] [real] NULL,
	[ModifiedAt] [datetime] NULL,
 CONSTRAINT [PK_StudyDisciplines] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[StudyForms]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StudyForms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[FormName] [nvarchar](100) NULL,
	[FormType] [int] NULL,
	[ModifiedAt] [datetime] NULL,
	[PaymentType] [int] NULL,
	[PaymentTypeName] [nvarchar](100) NULL,
 CONSTRAINT [PK_StudyForms] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[StudyPeriods]    Script Date: 26.04.2018 5:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StudyPeriods](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[FormName] [nvarchar](max) NULL,
	[FormType] [int] NULL,
	[ModifiedAt] [datetime] NULL,
	[Period] [int] NULL,
 CONSTRAINT [PK_StudyPeriods] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO

ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO

ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[Cathedras]  WITH CHECK ADD  CONSTRAINT [FK__Cathedras__Cathe__74AE54BC] FOREIGN KEY([CathedraHeadId])
REFERENCES [dbo].[Lecturers] ([Id])
GO

ALTER TABLE [dbo].[Cathedras] CHECK CONSTRAINT [FK__Cathedras__Cathe__74AE54BC]
GO

ALTER TABLE [dbo].[Cathedras]  WITH CHECK ADD  CONSTRAINT [FK__Cathedras__Commu__73BA3083] FOREIGN KEY([CommunicationDetailsId])
REFERENCES [dbo].[CommunicationDetails] ([Id])
GO

ALTER TABLE [dbo].[Cathedras] CHECK CONSTRAINT [FK__Cathedras__Commu__73BA3083]
GO

ALTER TABLE [dbo].[Cathedras]  WITH CHECK ADD  CONSTRAINT [FK__Cathedras__Facul__75A278F5] FOREIGN KEY([FacultyId])
REFERENCES [dbo].[Faculties] ([Id])
GO

ALTER TABLE [dbo].[Cathedras] CHECK CONSTRAINT [FK__Cathedras__Facul__75A278F5]
GO

ALTER TABLE [dbo].[Cathedras]  WITH CHECK ADD  CONSTRAINT [FK_Cathedras_LocationDetails_LocationDetailsId] FOREIGN KEY([LocationDetailsId])
REFERENCES [dbo].[LocationDetails] ([Id])
GO

ALTER TABLE [dbo].[Cathedras] CHECK CONSTRAINT [FK_Cathedras_LocationDetails_LocationDetailsId]
GO

ALTER TABLE [dbo].[CathedrasStaff]  WITH CHECK ADD  CONSTRAINT [FK__Cathedras__Cathe__787EE5A0] FOREIGN KEY([CathedraId])
REFERENCES [dbo].[Cathedras] ([Id])
GO

ALTER TABLE [dbo].[CathedrasStaff] CHECK CONSTRAINT [FK__Cathedras__Cathe__787EE5A0]
GO

ALTER TABLE [dbo].[CathedrasStaff]  WITH CHECK ADD  CONSTRAINT [FK__Cathedras__Lectu__797309D9] FOREIGN KEY([LecturerId])
REFERENCES [dbo].[Lecturers] ([Id])
GO

ALTER TABLE [dbo].[CathedrasStaff] CHECK CONSTRAINT [FK__Cathedras__Lectu__797309D9]
GO

ALTER TABLE [dbo].[CommandArguments]  WITH CHECK ADD  CONSTRAINT [FK_CommandArguments_Commands_CommandId] FOREIGN KEY([CommandId])
REFERENCES [dbo].[Commands] ([Id])
GO

ALTER TABLE [dbo].[CommandArguments] CHECK CONSTRAINT [FK_CommandArguments_Commands_CommandId]
GO

ALTER TABLE [dbo].[Faculties]  WITH CHECK ADD  CONSTRAINT [FK__Faculties__Commu__6FE99F9F] FOREIGN KEY([CommunicationDetailsId])
REFERENCES [dbo].[CommunicationDetails] ([Id])
GO

ALTER TABLE [dbo].[Faculties] CHECK CONSTRAINT [FK__Faculties__Commu__6FE99F9F]
GO

ALTER TABLE [dbo].[Faculties]  WITH CHECK ADD  CONSTRAINT [FK__Faculties__DeanI__70DDC3D8] FOREIGN KEY([DeanId])
REFERENCES [dbo].[Lecturers] ([Id])
GO

ALTER TABLE [dbo].[Faculties] CHECK CONSTRAINT [FK__Faculties__DeanI__70DDC3D8]
GO

ALTER TABLE [dbo].[LocationDetails]  WITH CHECK ADD  CONSTRAINT [FK__LocationD__CityI__60A75C0F] FOREIGN KEY([CityId])
REFERENCES [dbo].[Cities] ([Id])
GO

ALTER TABLE [dbo].[LocationDetails] CHECK CONSTRAINT [FK__LocationD__CityI__60A75C0F]
GO

ALTER TABLE [dbo].[LocationDetails]  WITH CHECK ADD  CONSTRAINT [FK__LocationD__Count__619B8048] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Countries] ([Id])
GO

ALTER TABLE [dbo].[LocationDetails] CHECK CONSTRAINT [FK__LocationD__Count__619B8048]
GO

ALTER TABLE [dbo].[Specialities]  WITH CHECK ADD  CONSTRAINT [FK__Specialit__Cathe__02FC7413] FOREIGN KEY([CathedraId])
REFERENCES [dbo].[Cathedras] ([Id])
GO

ALTER TABLE [dbo].[Specialities] CHECK CONSTRAINT [FK__Specialit__Cathe__02FC7413]
GO

ALTER TABLE [dbo].[Specialities]  WITH CHECK ADD  CONSTRAINT [FK__Specialit__Facul__02084FDA] FOREIGN KEY([FacultyId])
REFERENCES [dbo].[Faculties] ([Id])
GO

ALTER TABLE [dbo].[Specialities] CHECK CONSTRAINT [FK__Specialit__Facul__02084FDA]
GO

ALTER TABLE [dbo].[SpecialityActivityFields]  WITH CHECK ADD  CONSTRAINT [FK__Specialit__Activ__08B54D69] FOREIGN KEY([ActivityFieldId])
REFERENCES [dbo].[ActivityFields] ([Id])
GO

ALTER TABLE [dbo].[SpecialityActivityFields] CHECK CONSTRAINT [FK__Specialit__Activ__08B54D69]
GO

ALTER TABLE [dbo].[SpecialityActivityFields]  WITH CHECK ADD  CONSTRAINT [FK__Specialit__Speci__09A971A2] FOREIGN KEY([SpecialityId])
REFERENCES [dbo].[Specialities] ([Id])
GO

ALTER TABLE [dbo].[SpecialityActivityFields] CHECK CONSTRAINT [FK__Specialit__Speci__09A971A2]
GO

ALTER TABLE [dbo].[SpecialityStudyDisciplines]  WITH CHECK ADD  CONSTRAINT [FK__Specialit__Speci__0D7A0286] FOREIGN KEY([SpecialityId])
REFERENCES [dbo].[Specialities] ([Id])
GO

ALTER TABLE [dbo].[SpecialityStudyDisciplines] CHECK CONSTRAINT [FK__Specialit__Speci__0D7A0286]
GO

ALTER TABLE [dbo].[SpecialityStudyDisciplines]  WITH CHECK ADD  CONSTRAINT [FK__Specialit__Study__0C85DE4D] FOREIGN KEY([StudyDisciplineId])
REFERENCES [dbo].[StudyDisciplines] ([Id])
GO

ALTER TABLE [dbo].[SpecialityStudyDisciplines] CHECK CONSTRAINT [FK__Specialit__Study__0C85DE4D]
GO

ALTER TABLE [dbo].[SpecialityStudyPeriods]  WITH CHECK ADD  CONSTRAINT [FK__Specialit__Speci__1332DBDC] FOREIGN KEY([SpecialityId])
REFERENCES [dbo].[Specialities] ([Id])
GO

ALTER TABLE [dbo].[SpecialityStudyPeriods] CHECK CONSTRAINT [FK__Specialit__Speci__1332DBDC]
GO

ALTER TABLE [dbo].[SpecialityStudyPeriods]  WITH CHECK ADD  CONSTRAINT [FK__Specialit__Study__123EB7A3] FOREIGN KEY([StudyPeriodId])
REFERENCES [dbo].[StudyPeriods] ([Id])
GO

ALTER TABLE [dbo].[SpecialityStudyPeriods] CHECK CONSTRAINT [FK__Specialit__Study__123EB7A3]
GO

ALTER TABLE [dbo].[Specializations]  WITH CHECK ADD  CONSTRAINT [FK__Specializ__Speci__05D8E0BE] FOREIGN KEY([SpecialityId])
REFERENCES [dbo].[Specialities] ([Id])
GO

ALTER TABLE [dbo].[Specializations] CHECK CONSTRAINT [FK__Specializ__Speci__05D8E0BE]
GO
