USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'StudyPeriods', N'U') IS NULL )
BEGIN
	CREATE TABLE [StudyPeriods](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[FormType] INT,
		[FormName] NVARCHAR(MAX),
		[Period] INT,
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME
	)
END

GO