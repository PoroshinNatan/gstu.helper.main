USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'ActivityFields', N'U') IS NULL )
BEGIN
	CREATE TABLE [ActivityFields](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[Name] NVARCHAR(MAX),
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME
	)
END

GO