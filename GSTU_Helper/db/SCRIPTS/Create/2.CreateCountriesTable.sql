USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'Countries', N'U') IS NULL )
BEGIN
	CREATE TABLE [Countries](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[Name] NVARCHAR(50) NOT NULL,
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME
	)
END

GO