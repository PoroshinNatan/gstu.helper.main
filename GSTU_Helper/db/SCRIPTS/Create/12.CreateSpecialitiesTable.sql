USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'Specialities', N'U') IS NULL )
BEGIN
	CREATE TABLE [Specialities](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[Name] NVARCHAR(MAX),
		[Code] NVARCHAR(50),
		[Qualification] NVARCHAR(MAX),
		[FacultyId] INT,
		[CathedraId] INT,
		[Description] NTEXT,
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME,
		FOREIGN KEY(FacultyId) REFERENCES [Faculties](Id),
		FOREIGN KEY(CathedraId) REFERENCES [Cathedras](Id)
	)
END

GO