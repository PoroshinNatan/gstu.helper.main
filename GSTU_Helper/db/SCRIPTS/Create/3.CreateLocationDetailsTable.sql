USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'LocationDetails', N'U') IS NULL )
BEGIN
	CREATE TABLE [LocationDetails](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[CityId] INT,
		[CountryId] INT,
		[Address] NVARCHAR(MAX),
		[MailIndex] NVARCHAR(20),
		[Room] NVARCHAR(20),
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME,
		FOREIGN KEY(CityId) REFERENCES [Cities](Id),
		FOREIGN KEY(CountryId) REFERENCES [Countries](Id)
	)
END

GO