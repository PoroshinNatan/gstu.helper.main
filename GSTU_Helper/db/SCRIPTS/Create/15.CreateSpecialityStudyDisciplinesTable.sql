USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'SpecialityStudyDisciplines', N'U') IS NULL )
BEGIN
	CREATE TABLE [SpecialityStudyDisciplines](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[StudyDisciplineId] INT,
		[SpecialityId] INT,
		FOREIGN KEY(StudyDisciplineId) REFERENCES [StudyDisciplines](Id),
		FOREIGN KEY(SpecialityId) REFERENCES [Specialities](Id)
	)
END

GO