USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'Lecturers', N'U') IS NULL )
BEGIN
	CREATE TABLE [Lecturers](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[FirstName] NVARCHAR(MAX),
		[LastName] NVARCHAR(MAX),
		[Gender] INT,
		[ScientistGrade] INT,
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME
	)
END

GO