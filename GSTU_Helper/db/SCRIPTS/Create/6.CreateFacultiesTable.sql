USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'Faculties', N'U') IS NULL )
BEGIN
	CREATE TABLE [Faculties](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[ShortName] NVARCHAR(10),
		[FullName] NVARCHAR(100),
		[Description] NTEXT,
		[CommunicationDetailsId] INT,
		[WebSiteUrl] NVARCHAR(MAX),
		[DeanId] INT,
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME,
		FOREIGN KEY(CommunicationDetailsId) REFERENCES [CommunicationDetails](Id),
		FOREIGN KEY(DeanId) REFERENCES [Lecturers](Id)
	)
END

GO