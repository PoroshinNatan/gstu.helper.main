USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'StudyForms', N'U') IS NULL )
BEGIN
	CREATE TABLE [StudyForms](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[FormType] INT,
		[FormName] NVARCHAR(100),
		[PaymentType] INT,
		[PaymentTypeName] NVARCHAR(100),
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME
	)
END

GO