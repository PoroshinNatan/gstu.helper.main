USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'SpecialityStudyPeriods', N'U') IS NULL )
BEGIN
	CREATE TABLE [SpecialityStudyPeriods](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[StudyPeriodId] INT,
		[SpecialityId] INT,
		FOREIGN KEY(StudyPeriodId) REFERENCES [StudyPeriods](Id),
		FOREIGN KEY(SpecialityId) REFERENCES [Specialities](Id)
	)
END

GO