USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'Cathedras', N'U') IS NULL )
BEGIN
	CREATE TABLE [Cathedras](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[Name] NVARCHAR(50),
		[CommunicationDetailsId] INT,
		[LocationDetailsId] INT,
		[CathedraHeadId] INT,
		[FacultyId] INT,
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME,
		FOREIGN KEY(CommunicationDetailsId) REFERENCES [CommunicationDetails](Id),
		FOREIGN KEY(CathedraHeadId) REFERENCES [Lecturers](Id),
		FOREIGN KEY(FacultyId) REFERENCES [Faculties](Id),
		FOREIGN KEY(LocationDetailsId) REFERENCES [LocationDetails](Id)
	)
END

GO