USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'Specializations', N'U') IS NULL )
BEGIN
	CREATE TABLE [Specializations](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[Code] NVARCHAR(50),
		[Name] NVARCHAR(100),
		[SpecialityId] INT,
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME,
		FOREIGN KEY(SpecialityId) REFERENCES [Specialities](Id)
	)
END

GO