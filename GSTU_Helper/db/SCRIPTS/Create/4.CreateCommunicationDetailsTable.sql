USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'CommunicationDetails', N'U') IS NULL )
BEGIN
	CREATE TABLE [CommunicationDetails](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[Phone] NVARCHAR(20),
		[Email] NVARCHAR(50),
		[CreatedAt] DATETIME,
		[ModifiedAt] DATETIME
	)
END

GO