USE [GstuHelperDb]

GO

IF(OBJECT_ID (N'CathedrasStaff', N'U') IS NULL )
BEGIN
	CREATE TABLE [CathedrasStaff](
		[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
		[CathedraId] INT NOT NULL,
		[LecturerId] INT NOT NULL,
		FOREIGN KEY(CathedraId) REFERENCES [Cathedras](Id),
		FOREIGN KEY(LecturerId) REFERENCES [Lecturers](Id)
	)
END

GO