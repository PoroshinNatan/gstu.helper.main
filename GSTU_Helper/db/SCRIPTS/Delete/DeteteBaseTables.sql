USE [GstuHelperDb]

GO

DROP TABLE [SpecialityStudyPeriods]

GO

DROP TABLE [StudyPeriods]

GO

DROP TABLE [SpecialityStudyDisciplines]

GO

DROP TABLE [SpecialityActivityFields]

GO

DROP TABLE [Specializations]

GO

DROP TABLE [Specialities]

GO

DROP TABLE [ActivityFields]

GO

DROP TABLE [StudyDisciplines]

GO

DROP TABLE [StudyForms]

GO

DROP TABLE [CathedrasStaff]

GO

DROP TABLE [Cathedras]

GO

DROP TABLE [Faculties]

GO

DROP TABLE [Lecturers]

GO

DROP TABLE [CommunicationDetails]

GO

DROP TABLE [LocationDetails]

GO

DROP TABLE [Countries]

GO

DROP TABLE [Cities]

GO